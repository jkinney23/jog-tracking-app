from django.db import models


class WeatherConditions(models.Model):
    weather = models.CharField(max_length=50)
    description = models.CharField(max_length=200)
    
    
class Jog(models.Model):
    jogger = models.ForeignKey('auth.User')
    date = models.DateTimeField()
    distance = models.IntegerField()
    time_duration = models.IntegerField()
    location = models.CharField(max_length=200)
    weather = models.ForeignKey('WeatherConditions')
